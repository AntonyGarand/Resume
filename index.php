<?php 
    /*
     * This website follows the google styleguide found here: https://google.github.io/styleguide/htmlcssguide.xml
     * Optional tags are therefore not included (Ex: html, head, body
     *
     * Built by: Antony Garnad
     * On: 19th September 2016
     * Last updated: 3rd January 2018
     */
    require_once('config.php');
    require_once('functions.php');
?><!DOCTYPE html>
<!--
  -- Welcome to my awesome resume!
  -- The source is available at https://gitlab.com/AntonyGarand/Resume
  --
  -- Made with PHP and Javascript, html and CSS
  -->
<title><?=__("Antony Garand's Resume")?></title>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet'>
<?= addCss('style.css') ?>
<?= addScript('script.js') ?>
<div id="content">
    <div id="sectionLeft">
        <div id="profileImageWrapper">
            <?= addImage('me.png',__('Portrait of myself'), array('profileImage')) ?>
        </div>
        <div id="nameUpper"><?=__('Antony')?></div>
        <div id="nameLower"><?=__('Garand')?></div>
        <div class="subImageInformation"><a href="https://garand.info/resume"><?=__('https://garand.info/resume')?></a></div>
        <div class="subImageInformation"><a href="mailto:antonygarand@gmail.com"><?=__('AntonyGarand@gmail.com')?></a></div>
        <div class="left section">
            <h2 class="leftSectionTitle"><?=__('Skills')?></h2>
            <div>
                <h3 class="skillTitle"><?=__('Development')?></h3>
                <div class="skillProgressBar" name="JS / TS" value="8"></div>
                <div class="skillProgressBar" name="PHP" value="8"></div>
                <div class="skillProgressBar" name="C# / Java" value="8"></div>
                <div class="skillProgressBar" name="SQL" value="7"></div>
                <div class="skillProgressBar" name="Java" value="6"></div>
                <div class="skillProgressBar" name="Python" value="5"></div>
            </div>
            <div>
                <h3 class="skillTitle"><?=__('Misc')?></h3>
                <div class="skillProgressBar" name="<?=__('French')?>" value="10"></div>
                <div class="skillProgressBar" name="<?=__('Web')?>" value="8"></div>
                <div class="skillProgressBar" name="<?=__('Git')?>" value="8"></div>
            </div>
            <div>
                <h3 class="skillTitle"><?=__('Technologies')?></h3>
                <div class="skillRowWrapper">
                    <div class="skillRow firstRow">TypeScript</div>
                    <div class="skillRow secondRow">VueJS Angular</div>
                    <div class="skillRow thirdRow">Photoshop Office</div>
                    <div class="skillRow forthRow">WordPress ASP.net</div>
                    <div class="skillRow fifthRow">Vim Unity3D PWA Android</div>
                </div>
            </div>
        </div>
    </div>

    <div id="sectionRight">
        <div>
            <h1 id="experience" class="sectionRightTitle"><?=__('Experience')?></h1>
            <div class="workExperience">
                <!-- CGI -->
                <h2 class="sectionRightSubtitle">2017</h2>
                <p class="workRole role"><?=__('CGI - FullStack Developper')?></p>
                <p class="workDescription description">
                  <?=__('Lead Developper of a high performance retail solution')?>
                  <br/>
                  <?=__('Technologies: Java, Angular 2-4, Typescript, Couchbase, Redis')?>
                </p>
                <p class="workTasks tasks"><?=__('Tasks and responsibilities:')?></p>
                <ul class="workTasklist tasklist">
                    <li><?=__('Improve the codebase quality, Front-End and Back-End')?></li>
                    <li><?=__('Perform a technical analysis of our application and implement a micro-service architecture')?></li>
                    <li><?=__('Build critical pieces of the application following strict performance requirements')?></li>
                </ul>
            </div>
            <div class="workExperience">
                <!-- Sucuri -->
                <h2 class="sectionRightSubtitle">2015 - 2017</h2>
                <p class="remote"><?=__('Remote')?></p>
                <p class="workRole role"><?=__('Sucuri - Vulnerability Researcher')?></p>
                <p class="workDescription description">
                  <?=__('Searching for vulnerabilities within popular CMS such as WordPress, Joomla and Drupal')?>
                </p>
                <p class="workTasks tasks"><?=__('Tasks and responsibilities:')?></p>
                <ul class="workTasklist tasklist">
                    <li><?=__('Static and Dynamic Source code analysis')?></li>
                    <li><?=__('Application audit, black-box and white-box')?></li>
                    <li><?=__('Automation of various tasks related to the Firewall (CloudProxy)')?></li>
                </ul>
            </div>
        </div>
        <div id="extraWrapper">
            <h1 id="extra" class="sectionRightTitle"><?=__('Extra')?></h1>
            <div>
                <h2 class="sectionRightSubtitle">2014 - 2017</h2>
                <p class="extraRole role"><?=__('Security and Programming competitions')?></p>
                <p class="extraDescription description"><?=__('Participation to various competitions, physical and online, notably: ')?></p>
                <ul class="tasklist">
                    <li><?=__('2014 - Today - Hackfest')?></li>
                    <li><?=__('2014 - Today - Northsec')?></li>
                    <li><?=__('2017 - HackQC')?></li>
                    <li><?=__('2015 - Defcon')?></li>
                    <li><?=__('2015 - 2016 -  Facebook Hackercup')?></li>
                    <li><?=__('2015 - GoSecure (Finalist)')?></li>
                </ul>
            </div>
            <div>
                <h2 class="sectionRightSubtitle">2014 - 2016</h2>
                <p class="extraRole role"><?=__('Social and ecological commitee')?></p>
                <p class="extraDescription description">
                  <?=__('Active member of this commitee, with participation in various projects such as the abolition of water bottles, LGBTQ + awareness and more.')?>
                </p>
            </div>
        </div>
        <div class="source"><?=__('Source available on')?> <a href="https://gitlab.com/AntonyGarand/Resume">Gitlab</a></div>
    </div>
</div>
